import {Profile} from '../models/coppel.models'

export default {
  getAll: async (request, response, next) => {
    const profiles = await Profile.findAll();
    //console.log(employers.every(employer => Employeer instanceof Employeer)); // true
    //console.log("All users:", JSON.stringify(employers, null, 2));
    response.status(200).json(profiles)
  }
}
