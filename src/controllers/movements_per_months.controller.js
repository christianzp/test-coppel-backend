import {Employeer, MovementPerMonth, Profile} from '../models/coppel.models'
import {BASE, DELIVERY, ISR, ISR_PLUS, TAXS_OVER_MONTH, VOUCHES} from '../utils/constants'

export default {
  getAll: async (request, response, next) => {
    const employers = await Employeer.findAll();
    //console.log(employers.every(employer => Employeer instanceof Employeer)); // true
    //console.log("All users:", JSON.stringify(employers, null, 2));
    response.status(200).json(employers)
  },

  getById: async (request, response, next) => {
    try{
      const {id} = request.params
      const employer = await Employeer.findByPk(id)
      response.status(200).json(employer)  
    }catch(err){
      response.status(500).json({message: err.message})
    }
  },

  create: async (request, response, next) => {
    try{
      const {employeer_id, deliveries, hours} = request.body
      const employer = await Employeer.findByPk(employeer_id);
      const profile = await Profile.findByPk(employer.profile_id)
      let total_balance = 0.0;
      let val_deli = deliveries * DELIVERY // pago total por entregas
      let balance_base = hours * BASE; //sueldo base (hrs * 30)
      
      total_balance = total_balance + balance_base; //total + sueldo base
      total_balance = total_balance + val_deli; // total + bono por entrega (5 * entregas)
      total_balance = total_balance + profile.base // total + bono de perfil
      let isr = total_balance * ISR // se calcula el ISR
      total_balance = total_balance - isr; // total - isr

      let irs_plus = 0.0;
      if(total_balance >= TAXS_OVER_MONTH){
        irs_plus = total_balance * ISR_PLUS;
        total_balance = total_balance - irs_plus
      }
      let retentions = irs_plus + isr;  // Suma de los impuestos que se descontaron
      let vouchers = total_balance * VOUCHES; // calculo de bonos de despensa

      const new_movement = await MovementPerMonth.create({
        employeer_id,
        base: balance_base,
        total_hours: hours,
        retentions,
        vouchers,
        total_balance_deliveries: val_deli,
        total_balance_final: total_balance

      })
      response.status(201).json(new_movement);
    }catch(err){
      response.status(500).json({message: err.message})
    }
  },

  update: async (request, response, next) => {
    try{
      const {id} = request.params
      const {profile_id} = request.body
      const employer = await Employeer.update({profile_id},{where:  {id}})
      response.status(204).json(employer)
    }catch(err){
      response.status(500).json({message: err.message})
    }
  },

  delete: async (request, response, next) => {
  }
}
