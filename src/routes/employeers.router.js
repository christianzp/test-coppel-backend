import routerx from 'express-promise-router'
import  employeersController  from '../controllers/employeers.controller'

const router = routerx();

router.get('/', employeersController.getAll);
router.get('/:id', employeersController.getById);
router.post('/', employeersController.create);
router.put('/:id', employeersController.update);
router.delete('/:id', employeersController.delete);

export default router;
