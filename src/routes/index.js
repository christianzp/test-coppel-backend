import routerx from 'express-promise-router'
import employeersRoutes from '../routes/employeers.router';
import profilesRoutes from '../routes/profiles.router';
import movements_per_monthsRoutes from '../routes/movements_per_months.router';

const router = routerx();

router.use('/employeers', employeersRoutes);
router.use('/profiles', profilesRoutes);
router.use('/movements', movements_per_monthsRoutes);

export default router;

