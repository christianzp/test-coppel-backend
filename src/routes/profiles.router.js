import routerx from 'express-promise-router'
import profilesController  from '../controllers/profiles.controller'

const router = routerx();

router.get('/', profilesController.getAll);

export default router;
