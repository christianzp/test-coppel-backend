import routerx from 'express-promise-router'
import movementsController  from '../controllers/movements_per_months.controller'

const router = routerx();

router.post('/', movementsController.create);

export default router;
