require('dotenv').config();
import app from './app'
import sequelize from './config/squalize'

async function main(){
    process.env.PORT = process.env.PORT || "9966"
    try{
        //await sequelize.authenticate();
        await sequelize.sync({force: true});
        console.log('Connection has been established successfully.');
        app.listen(process.env.PORT, async () => {
            console.log(`⚡️[server]: Server is running at http://localhost:${process.env.PORT}/test-coopel.app`)
            console.log(`⚡️[server]: Api docs is running at http://localhost:${process.env.PORT}/test-coppel.api-docs`)
        });
    }catch(err){
        console.log('Unable to connect to the database: ', err);

    }
}

main();


