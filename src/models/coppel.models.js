import { DataTypes, Sequelize } from 'sequelize';

import sequelize from '../config/squalize/index';

export const Profile = sequelize.define('profiles', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    description: { type: DataTypes.TEXT },
    base: { type: DataTypes.FLOAT },
}, { timestamps : true});

export const Employeer = sequelize.define('employeers', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    "name": { type: DataTypes.TEXT },
    lastname: { type: DataTypes.TEXT },
    profile_id: { type: DataTypes.INTEGER, allowNull: false },
}, { timestamps: true });

export const MovementPerMonth = sequelize.define('movements_per_months', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    employeer_id: { type: DataTypes.INTEGER, allowNull: false },
    base: { type: DataTypes.FLOAT },
    total_hours	 : { type: DataTypes.TEXT },	
    retentions	:{ type: DataTypes.FLOAT },
    vouchers	:{ type: DataTypes.FLOAT },
    total_balance_final	:{ type: DataTypes.FLOAT },
    total_balance_deliveries:	{ type: DataTypes.FLOAT },
},{ timestamps: true });



Profile.hasOne(Employeer, {
    onDelete: 'RESTRICT',
    onUpdate: 'RESTRICT',
    foreignKey: {"name":  "profile_id"},
    sourceKey: "id"

});

Employeer.belongsTo(Profile,{
    foreignKey: {"name":  "profile_id"},
    sourceKey: 'id'
})

/*MovementPerMonth.belongsTo(Employeer,{
    foreignKey: 'employer_id',
    sourceKey: 'id'
})*/
