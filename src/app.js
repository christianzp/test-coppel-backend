import bodyParser from 'body-parser'
import cors from 'cors'
import express from 'express';
import fileUpload from 'express-fileupload';
import morgan from 'morgan'
import router from './routes/index'
import YAML from 'yamljs';
import swaggerUi from 'swagger-ui-express'

const swaggerDocument = YAML.load('./swagger.yaml');
const app = express();
app.use(morgan('dev'));
app.use(cors());
app.use(fileUpload());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/test-coppel.api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))
app.use("/test-coopel.app", router);

export default app;