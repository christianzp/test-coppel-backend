"use strict";

var _bodyParser = _interopRequireDefault(require("body-parser"));
var _cors = _interopRequireDefault(require("cors"));
var _express = _interopRequireDefault(require("express"));
var _expressFileupload = _interopRequireDefault(require("express-fileupload"));
var _morgan = _interopRequireDefault(require("morgan"));
var _index = _interopRequireDefault(require("./routes/index"));
var _yamljs = _interopRequireDefault(require("yamljs"));
var _swaggerUiExpress = _interopRequireDefault(require("swagger-ui-express"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
require('dotenv').config();
const swaggerDocument = _yamljs.default.load('./swagger.yaml');
const app = (0, _express.default)();
process.env.PORT = process.env.PORT || "9966";
app.use((0, _morgan.default)('dev'));
app.use((0, _cors.default)());
app.use((0, _expressFileupload.default)());
app.use(_bodyParser.default.urlencoded({
  extended: false
}));
app.use(_bodyParser.default.json());
app.use('/test-coppel.api-docs', _swaggerUiExpress.default.serve, _swaggerUiExpress.default.setup(swaggerDocument));
app.use("/test-coopel.app", _index.default);
app.listen(process.env.PORT, async () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${process.env.PORT}/test-coopel.app`);
  console.log(`⚡️[server]: Api docs is running at http://localhost:${process.env.PORT}/test-coppel.api-docs`);
});